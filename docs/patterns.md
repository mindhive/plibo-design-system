### Patterns are UI Patterns that fall on the more complex side of the spectrum. Date pickers, data tables, and visualizations are good examples. Patterns utilize both elements and design tokens. If you wonder whether something should be called an element or a pattern, ask yourself this question: _“Can this be broken down into smaller pieces?”_ If the answer is yes, it should most likely be a pattern.

## Overview

```
<components show="patterns" />
```

### Design tokens are the atoms of the system. They are used instead of hard coded values to ensure a better consistency across any platform.

## Color Palette

```
<color />
```

## Font Sizes

```
<font-size />
```

## Spacing

```
<spacing />
```

## All Tokens

```
<all />
```

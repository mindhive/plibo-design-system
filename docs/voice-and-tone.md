### Through this guide you will learn how to apply Vue Design System’s voice and choose the right tone. Using the right voice and tone is important as it allows us to better connect with our users.

## Crisp and clear

To edit or remove this guideline, see [/docs/voice-and-tone.md](https://github.com/viljamis/vue-design-system/blob/master/docs/principles.md).

## Empathetic

To edit or remove this guideline, see [/docs/voice-and-tone.md](https://github.com/viljamis/vue-design-system/blob/master/docs/voice-and-tone.md).

## Confident, but not arrogant

To edit or remove this guideline, see [/docs/voice-and-tone.md](https://github.com/viljamis/vue-design-system/blob/master/docs/voice-and-tone.md).

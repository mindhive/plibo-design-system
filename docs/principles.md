### Principles are the foundation of the system. They form the basis of a good product and help the team with decision making. They are here to guide you and your team when working with the myriad parts of the system and help you do better and more informed decisions.

## 1. Design is how it works

To edit or remove this principle, see [/docs/principles.md](https://github.com/viljamis/vue-design-system/blob/master/docs/principles.md).

## 2. Reduce concepts to increase confidence

To edit or remove this principle, see [/docs/principles.md](https://github.com/viljamis/vue-design-system/blob/master/docs/principles.md).

## 3. Design with real data

To edit or remove this principle, see [/docs/principles.md](https://github.com/viljamis/vue-design-system/blob/master/docs/principles.md).

## 4. Design systems, not pages

To edit or remove this principle, see [/docs/principles.md](https://github.com/viljamis/vue-design-system/blob/master/docs/principles.md).

## 5. Involve code early

To edit or remove this principle, see [/docs/principles.md](https://github.com/viljamis/vue-design-system/blob/master/docs/principles.md).

## 6. Fast is better than slow

To edit or remove this principle, see [/docs/principles.md](https://github.com/viljamis/vue-design-system/blob/master/docs/principles.md).

## 7. Let content determine breakpoints

To edit or remove this principle, see [/docs/principles.md](https://github.com/viljamis/vue-design-system/blob/master/docs/principles.md).

## 8. Strive for universality

To edit or remove this principle, see [/docs/principles.md](https://github.com/viljamis/vue-design-system/blob/master/docs/principles.md).

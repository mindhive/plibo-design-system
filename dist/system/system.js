module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "+KGH":
/*!******************************************************!*\
  !*** ./src/templates/FeatureSection.vue + 4 modules ***!
  \******************************************************/
/*! exports provided: default */
/*! ModuleConcatenation bailout: Cannot concat with ./src/templates/FeatureSection.vue?vue&type=custom&index=0&blockType=docs (<- Module is not an ECMAScript module) */
/*! ModuleConcatenation bailout: Cannot concat with ./node_modules/vue-loader/lib/runtime/componentNormalizer.js because of ./src/elements/Button.vue */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/templates/FeatureSection.vue?vue&type=template&id=508f391e&scoped=true&
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "section section--features" }, [
    _c(
      "div",
      { staticClass: "container" },
      [
        _c("Heading", { attrs: { level: "h3", align: "center" } }, [
          _vm._v("Marvel Characters")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c(
              "div",
              { staticClass: "col-12 col-md-6 col-lg-4 text-center" },
              [
                _c(
                  "FeatureCard",
                  {
                    attrs: {
                      image:
                        "https://cdn4.iconfinder.com/data/icons/famous-characters-add-on-vol-1-flat/48/Famous_Character_-_Add_On_1-24-512.png",
                      heading: "Wolverine"
                    }
                  },
                  [
                    _vm._v(
                      "\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non semper dolor.\n          "
                    )
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col-12 col-md-6 col-lg-4 text-center" },
              [
                _c(
                  "FeatureCard",
                  {
                    attrs: {
                      image:
                        "https://cdn4.iconfinder.com/data/icons/famous-characters-add-on-vol-1-flat/48/Famous_Character_-_Add_On_1-22-512.png",
                      heading: "Batman"
                    }
                  },
                  [
                    _vm._v(
                      "\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non semper dolor.\n          "
                    )
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass:
                  "col-12 col-md-6 offset-md-3 col-lg-4 offset-lg-0 text-center"
              },
              [
                _c(
                  "FeatureCard",
                  {
                    attrs: {
                      image:
                        "https://cdn4.iconfinder.com/data/icons/famous-characters-add-on-vol-1-flat/48/Famous_Character_-_Add_On_1-15-512.png",
                      heading: "Deadpool"
                    }
                  },
                  [
                    _vm._v(
                      "\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non semper dolor.\n          "
                    )
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass:
                  "col-12 col-md-6 offset-md-3 col-lg-4 offset-lg-0 text-center"
              },
              [
                _c(
                  "FeatureCard",
                  {
                    attrs: {
                      image:
                        "https://cdn4.iconfinder.com/data/icons/famous-characters-add-on-vol-1-flat/48/Famous_Character_-_Add_On_1-09-512.png",
                      heading: "Captain America"
                    }
                  },
                  [
                    _vm._v(
                      "\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non semper dolor.\n          "
                    )
                  ]
                )
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _vm._t("footer")
      ],
      2
    )
  ])
}
var staticRenderFns = []
render._withStripped = true


// CONCATENATED MODULE: ./src/templates/FeatureSection.vue?vue&type=template&id=508f391e&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/templates/FeatureSection.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/**
 * Multicolumn layout of FeatureCard elements instances.
 */
/* harmony default export */ var FeatureSectionvue_type_script_lang_js_ = ({
  name: "FeatureSection",
  status: "ready",
  release: "1.0.0"
});
// CONCATENATED MODULE: ./src/templates/FeatureSection.vue?vue&type=script&lang=js&
 /* harmony default export */ var templates_FeatureSectionvue_type_script_lang_js_ = (FeatureSectionvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/templates/FeatureSection.vue?vue&type=style&index=0&id=508f391e&lang=scss&scoped=true&
var FeatureSectionvue_type_style_index_0_id_508f391e_lang_scss_scoped_true_ = __webpack_require__("uN3n");

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("KHd+");

// EXTERNAL MODULE: ./src/templates/FeatureSection.vue?vue&type=custom&index=0&blockType=docs
var FeatureSectionvue_type_custom_index_0_blockType_docs = __webpack_require__("iBhG");
var FeatureSectionvue_type_custom_index_0_blockType_docs_default = /*#__PURE__*/__webpack_require__.n(FeatureSectionvue_type_custom_index_0_blockType_docs);

// CONCATENATED MODULE: ./src/templates/FeatureSection.vue






/* normalize component */

var component = Object(componentNormalizer["default"])(
  templates_FeatureSectionvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  "508f391e",
  null
  
)

/* custom blocks */

if (typeof FeatureSectionvue_type_custom_index_0_blockType_docs_default.a === 'function') FeatureSectionvue_type_custom_index_0_blockType_docs_default()(component)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/templates/FeatureSection.vue"
/* harmony default export */ var FeatureSection = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "+aZc":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader??ref--9-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/lib/loader.js??ref--9-3!./node_modules/sass-resources-loader/lib/loader.js??ref--9-4!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Paragraph.vue?vue&type=style&index=0&id=5a8076cf&lang=scss&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "/gSI":
/*!************************************************************************!*\
  !*** ./src/patterns/Picker.vue?vue&type=custom&index=0&blockType=docs ***!
  \************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports) {



/***/ }),

/***/ "/kPn":
/*!*********************************************!*\
  !*** ./src/elements/Button.vue + 4 modules ***!
  \*********************************************/
/*! exports provided: default */
/*! ModuleConcatenation bailout: Cannot concat with ./src/elements/Button.vue?vue&type=custom&index=0&blockType=docs (<- Module is not an ECMAScript module) */
/*! ModuleConcatenation bailout: Cannot concat with ./node_modules/vue-loader/lib/runtime/componentNormalizer.js because of ./src/elements/Heading.vue */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Button.vue?vue&type=template&id=08690ba1&scoped=true&
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    _vm.type,
    {
      tag: "component",
      class: ["button", _vm.size, _vm.state, _vm.variation],
      attrs: { href: _vm.href, type: _vm.submit }
    },
    [_vm._t("default")],
    2
  )
}
var staticRenderFns = []
render._withStripped = true


// CONCATENATED MODULE: ./src/elements/Button.vue?vue&type=template&id=08690ba1&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Button.vue?vue&type=script&lang=js&
//
//
//
//
//
//

/**
 * Buttons are generally used for interface actions. Suitable for all-purpose use.
 * Defaults to appearance that has white text on colored background, without border.
 * Primary style should be used only once per view for main call-to-action.
 */
/* harmony default export */ var Buttonvue_type_script_lang_js_ = ({
  name: "Button",
  status: "prototype",
  release: "1.0.0",
  props: {
    /**
     * The html element used for the button.
     * `button, a`
     */
    type: {
      type: String,
      default: "button",
      validator: function validator(value) {
        return value.match(/(button|a)/);
      }
    },

    /**
     * The size of the button. Defaults to medium.
     * `small, medium, large`
     */
    size: {
      type: String,
      default: "medium",
      validator: function validator(value) {
        return value.match(/(small|medium|large)/);
      }
    },

    /**
     * When setting the button’s type to a link, use this option to give a href.
     */
    href: {
      type: String,
      default: null
    },

    /**
     * Set the button’s type to “submit”.
     */
    submit: {
      type: String,
      default: null,
      validator: function validator(value) {
        return value.match(/(null|submit)/);
      }
    },

    /**
     * Manually trigger various states of the button.
     * `hover, active, focus`
     */
    state: {
      type: String,
      default: null,
      validator: function validator(value) {
        return value.match(/(hover|active|focus)/);
      }
    },

    /**
     * Style variation to give additional meaning.
     * `primary, secondary`
     */
    variation: {
      type: String,
      default: null,
      validator: function validator(value) {
        return value.match(/(primary|secondary|ghost)/);
      }
    }
  }
});
// CONCATENATED MODULE: ./src/elements/Button.vue?vue&type=script&lang=js&
 /* harmony default export */ var elements_Buttonvue_type_script_lang_js_ = (Buttonvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/elements/Button.vue?vue&type=style&index=0&id=08690ba1&lang=scss&scoped=true&
var Buttonvue_type_style_index_0_id_08690ba1_lang_scss_scoped_true_ = __webpack_require__("u8pL");

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("KHd+");

// EXTERNAL MODULE: ./src/elements/Button.vue?vue&type=custom&index=0&blockType=docs
var Buttonvue_type_custom_index_0_blockType_docs = __webpack_require__("tcIX");
var Buttonvue_type_custom_index_0_blockType_docs_default = /*#__PURE__*/__webpack_require__.n(Buttonvue_type_custom_index_0_blockType_docs);

// CONCATENATED MODULE: ./src/elements/Button.vue






/* normalize component */

var component = Object(componentNormalizer["default"])(
  elements_Buttonvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  "08690ba1",
  null
  
)

/* custom blocks */

if (typeof Buttonvue_type_custom_index_0_blockType_docs_default.a === 'function') Buttonvue_type_custom_index_0_blockType_docs_default()(component)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/elements/Button.vue"
/* harmony default export */ var Button = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "/thn":
/*!*****************************************!*\
  !*** ./src/assets/icons/deprecated.svg ***!
  \*****************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 512 512\"><path d=\"M497.9 150.5c9 9 14.1 21.2 14.1 33.9v143.1c0 12.7-5.1 24.9-14.1 33.9L361.5 497.9c-9 9-21.2 14.1-33.9 14.1H184.5c-12.7 0-24.9-5.1-33.9-14.1L14.1 361.5c-9-9-14.1-21.2-14.1-33.9V184.5c0-12.7 5.1-24.9 14.1-33.9L150.5 14.1c9-9 21.2-14.1 33.9-14.1h143.1c12.7 0 24.9 5.1 33.9 14.1l136.5 136.4zM377.6 338c4.7-4.7 4.7-12.3 0-17l-65-65 65.1-65.1c4.7-4.7 4.7-12.3 0-17L338 134.4c-4.7-4.7-12.3-4.7-17 0l-65 65-65.1-65.1c-4.7-4.7-12.3-4.7-17 0L134.4 174c-4.7 4.7-4.7 12.3 0 17l65.1 65.1-65.1 65.1c-4.7 4.7-4.7 12.3 0 17l39.6 39.6c4.7 4.7 12.3 4.7 17 0l65.1-65.1 65.1 65.1c4.7 4.7 12.3 4.7 17 0l39.4-39.8z\"/></svg>";

/***/ }),

/***/ 0:
/*!*****************************!*\
  !*** multi ./src/system.js ***!
  \*****************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./src/system.js */"5m5j");


/***/ }),

/***/ "1phr":
/*!**********************************************************************!*\
  !*** ./src/elements/Icon.vue?vue&type=custom&index=0&blockType=docs ***!
  \**********************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports) {



/***/ }),

/***/ "2dwG":
/*!****************************************!*\
  !*** ./src/assets/icons/prototype.svg ***!
  \****************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 448 512\"><path d=\"M400 480H48c-26.5 0-48-21.5-48-48V80c0-26.5 21.5-48 48-48h352c26.5 0 48 21.5 48 48v352c0 26.5-21.5 48-48 48zM238.1 177.9L102.4 313.6l-6.3 57.1c-.8 7.6 5.6 14.1 13.3 13.3l57.1-6.3L302.2 242c2.3-2.3 2.3-6.1 0-8.5L246.7 178c-2.5-2.4-6.3-2.4-8.6-.1zM345 165.1L314.9 135c-9.4-9.4-24.6-9.4-33.9 0l-23.1 23.1c-2.3 2.3-2.3 6.1 0 8.5l55.5 55.5c2.3 2.3 6.1 2.3 8.5 0L345 199c9.3-9.3 9.3-24.5 0-33.9z\"/></svg>";

/***/ }),

/***/ "5WVf":
/*!******************************************************************************************!*\
  !*** ./src/elements/Input.vue?vue&type=style&index=0&id=0c3078aa&lang=scss&scoped=true& ***!
  \******************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module exports are unknown */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Input_vue_vue_type_style_index_0_id_0c3078aa_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/mini-css-extract-plugin/dist/loader.js!../../node_modules/css-loader??ref--9-1!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src??ref--9-2!../../node_modules/sass-loader/lib/loader.js??ref--9-3!../../node_modules/sass-resources-loader/lib/loader.js??ref--9-4!../../node_modules/vue-loader/lib??vue-loader-options!./Input.vue?vue&type=style&index=0&id=0c3078aa&lang=scss&scoped=true& */ "Wg5T");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Input_vue_vue_type_style_index_0_id_0c3078aa_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Input_vue_vue_type_style_index_0_id_0c3078aa_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Input_vue_vue_type_style_index_0_id_0c3078aa_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Input_vue_vue_type_style_index_0_id_0c3078aa_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Input_vue_vue_type_style_index_0_id_0c3078aa_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "5m5j":
/*!***********************!*\
  !*** ./src/system.js ***!
  \***********************/
/*! exports provided: default */
/*! ModuleConcatenation bailout: Module is referenced from these modules with unsupported syntax: multi ./src/system.js (referenced with single entry) */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/**
 * System.js creates the Design System Library.
 * It’s used in the system itself and when exporting it.
 *
 * You should & can add your own dependencies here if needed.
 */
// Define contexts to require
var contexts = [__webpack_require__("Xl01"), __webpack_require__("JtIp"), __webpack_require__("c1KM")]; // Define components

var components = [];
contexts.forEach(function (context) {
  context.keys().forEach(function (key) {
    return components.push(context(key).default);
  });
}); // Install the above defined components

var System = {
  install: function install(Vue) {
    components.forEach(function (component) {
      return Vue.component(component.name, component);
    });
  }
}; // Automatic installation if Vue has been added to the global scope

if (typeof window !== "undefined" && window.Vue) {
  window.Vue.use(System);
} // Finally export as default


/* harmony default export */ __webpack_exports__["default"] = (System);

/***/ }),

/***/ "68yd":
/*!*******************************************!*\
  !*** ./src/elements/Icon.vue + 4 modules ***!
  \*******************************************/
/*! exports provided: default */
/*! ModuleConcatenation bailout: Cannot concat with ./src/elements/Icon.vue?vue&type=custom&index=0&blockType=docs (<- Module is not an ECMAScript module) */
/*! ModuleConcatenation bailout: Cannot concat with ./node_modules/vue-loader/lib/runtime/componentNormalizer.js because of ./src/elements/Button.vue */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Icon.vue?vue&type=template&id=801621b0&
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(_vm.type, {
    tag: "component",
    class: ["icon", _vm.size],
    attrs: { "aria-label": _vm.ariaLabel },
    domProps: { innerHTML: _vm._s(_vm.svg) }
  })
}
var staticRenderFns = []
render._withStripped = true


// CONCATENATED MODULE: ./src/elements/Icon.vue?vue&type=template&id=801621b0&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Icon.vue?vue&type=script&lang=js&
//
//
//
//
var req = __webpack_require__("bzL3");
/**
 * Icons are used to visually communicate core parts of the product and
 * available actions. They can act as wayfinding tools to help users more
 * easily understand where they are in the product.
 */


/* harmony default export */ var Iconvue_type_script_lang_js_ = ({
  name: "Icon",
  status: "review",
  release: "1.0.0",
  props: {
    /**
     * The name of the icon to display.
     */
    name: {
      required: true,
      default: "settings"
    },

    /**
     * The fill color of the SVG icon.
     */
    fill: {
      type: String,
      default: "currentColor"
    },

    /**
     * Descriptive text to be read to screenreaders.
     */
    ariaLabel: {
      type: String,
      default: "icon"
    },

    /**
     * The html element name used for the icon.
     */
    type: {
      type: String,
      default: "span"
    },

    /**
     * The size of the icon. Defaults to medium.
     * `small, medium, large`
     */
    size: {
      type: String,
      default: "medium",
      validator: function validator(value) {
        return value.match(/(small|medium|large)/);
      }
    }
  },
  data: function data() {
    return {
      svg: req("./" + this.name + ".svg").replace(/^<svg /, "<svg style=\"fill: ".concat(this.fill, "\" "))
    };
  }
});
// CONCATENATED MODULE: ./src/elements/Icon.vue?vue&type=script&lang=js&
 /* harmony default export */ var elements_Iconvue_type_script_lang_js_ = (Iconvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/elements/Icon.vue?vue&type=style&index=0&lang=scss&
var Iconvue_type_style_index_0_lang_scss_ = __webpack_require__("c5QI");

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("KHd+");

// EXTERNAL MODULE: ./src/elements/Icon.vue?vue&type=custom&index=0&blockType=docs
var Iconvue_type_custom_index_0_blockType_docs = __webpack_require__("1phr");
var Iconvue_type_custom_index_0_blockType_docs_default = /*#__PURE__*/__webpack_require__.n(Iconvue_type_custom_index_0_blockType_docs);

// CONCATENATED MODULE: ./src/elements/Icon.vue






/* normalize component */

var component = Object(componentNormalizer["default"])(
  elements_Iconvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* custom blocks */

if (typeof Iconvue_type_custom_index_0_blockType_docs_default.a === 'function') Iconvue_type_custom_index_0_blockType_docs_default()(component)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/elements/Icon.vue"
/* harmony default export */ var Icon = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "7dOS":
/*!***********************************************************************!*\
  !*** ./src/elements/Input.vue?vue&type=custom&index=0&blockType=docs ***!
  \***********************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports) {



/***/ }),

/***/ "8FB0":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader??ref--9-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/lib/loader.js??ref--9-3!./node_modules/sass-resources-loader/lib/loader.js??ref--9-4!./node_modules/vue-loader/lib??vue-loader-options!./src/templates/FeatureSection.vue?vue&type=style&index=0&id=508f391e&lang=scss&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "9Hea":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader??ref--9-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/lib/loader.js??ref--9-3!./node_modules/sass-resources-loader/lib/loader.js??ref--9-4!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Button.vue?vue&type=style&index=0&id=08690ba1&lang=scss&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "A5wI":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader??ref--9-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/lib/loader.js??ref--9-3!./node_modules/sass-resources-loader/lib/loader.js??ref--9-4!./node_modules/vue-loader/lib??vue-loader-options!./src/patterns/FeatureCard.vue?vue&type=style&index=0&id=29703033&lang=scss&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "AAsE":
/*!************************************!*\
  !*** ./src/assets/icons/ready.svg ***!
  \************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 512 512\"><path d=\"M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z\" class=\"st1\"/></svg>";

/***/ }),

/***/ "AsAg":
/*!*********************************************!*\
  !*** ./src/elements/Toggle.vue + 4 modules ***!
  \*********************************************/
/*! exports provided: default */
/*! ModuleConcatenation bailout: Cannot concat with ./src/elements/Toggle.vue?vue&type=custom&index=0&blockType=docs (<- Module is not an ECMAScript module) */
/*! ModuleConcatenation bailout: Cannot concat with ./node_modules/vue-loader/lib/runtime/componentNormalizer.js because of ./src/elements/Button.vue */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Toggle.vue?vue&type=template&id=7f8a827a&scoped=true&
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "span",
    { staticClass: "toggle", class: { "toggle--active": _vm.enabled } },
    [_vm._t("default")],
    2
  )
}
var staticRenderFns = []
render._withStripped = true


// CONCATENATED MODULE: ./src/elements/Toggle.vue?vue&type=template&id=7f8a827a&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Toggle.vue?vue&type=script&lang=js&
//
//
//
//

/**
 * Toggles are useful to represent a binary state. They can be used in many
 * various contexts and components as controls for flag state manipulation.
 */
/* harmony default export */ var Togglevue_type_script_lang_js_ = ({
  name: "Toggle",
  status: "ready",
  release: "1.0.0",
  props: {
    /**
     * Indicates current toggle state.
     */
    enabled: {
      type: Boolean,
      required: true,
      default: false
    }
  },
  methods: {
    toggle: function toggle() {
      this.$emit("toggle");
    }
  }
});
// CONCATENATED MODULE: ./src/elements/Toggle.vue?vue&type=script&lang=js&
 /* harmony default export */ var elements_Togglevue_type_script_lang_js_ = (Togglevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/elements/Toggle.vue?vue&type=style&index=0&id=7f8a827a&lang=scss&scoped=true&
var Togglevue_type_style_index_0_id_7f8a827a_lang_scss_scoped_true_ = __webpack_require__("jDvS");

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("KHd+");

// EXTERNAL MODULE: ./src/elements/Toggle.vue?vue&type=custom&index=0&blockType=docs
var Togglevue_type_custom_index_0_blockType_docs = __webpack_require__("nJDe");
var Togglevue_type_custom_index_0_blockType_docs_default = /*#__PURE__*/__webpack_require__.n(Togglevue_type_custom_index_0_blockType_docs);

// CONCATENATED MODULE: ./src/elements/Toggle.vue






/* normalize component */

var component = Object(componentNormalizer["default"])(
  elements_Togglevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  "7f8a827a",
  null
  
)

/* custom blocks */

if (typeof Togglevue_type_custom_index_0_blockType_docs_default.a === 'function') Togglevue_type_custom_index_0_blockType_docs_default()(component)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/elements/Toggle.vue"
/* harmony default export */ var Toggle = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "Cs02":
/*!*********************************************!*\
  !*** ./src/patterns/Picker.vue + 4 modules ***!
  \*********************************************/
/*! exports provided: default */
/*! ModuleConcatenation bailout: Cannot concat with ./src/patterns/Picker.vue?vue&type=custom&index=0&blockType=docs (<- Module is not an ECMAScript module) */
/*! ModuleConcatenation bailout: Cannot concat with ./node_modules/vue-loader/lib/runtime/componentNormalizer.js because of ./src/elements/Button.vue */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/patterns/Picker.vue?vue&type=template&id=509124de&scoped=true&
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      directives: [
        {
          name: "closable",
          rawName: "v-closable",
          value: { exclude: ["picker"], handler: "onBlur" },
          expression: "{ exclude: ['picker'], handler: 'onBlur' }"
        }
      ],
      ref: "picker",
      staticClass: "picker",
      class: { "picker--open": _vm.open },
      on: {
        blur: function($event) {
          _vm.alert("picker blurred")
        }
      }
    },
    [
      _c(
        "div",
        { staticClass: "picker__selection", on: { click: _vm.openPicker } },
        [
          !_vm.value.length
            ? _c("span", {
                staticClass: "picker__selection__greeter",
                domProps: { textContent: _vm._s(_vm.greeter) }
              })
            : _vm._e(),
          _vm._v(" "),
          _vm._l(_vm.displayValue, function(option, index) {
            return _c("span", {
              key: index,
              staticClass: "picker__selection__item",
              domProps: { textContent: _vm._s(option.label) }
            })
          }),
          _vm._v(" "),
          _vm.value.length > 3
            ? _c(
                "span",
                {
                  staticClass:
                    "picker__selection__item picker__selection__item--disabled"
                },
                [_vm._v("+ " + _vm._s(_vm.value.length - 3) + " więcej")]
              )
            : _vm._e()
        ],
        2
      ),
      _vm._v(" "),
      _c(
        "ul",
        { staticClass: "picker__options" },
        _vm._l(_vm.options, function(option, index) {
          return _c(
            "li",
            {
              key: index,
              on: {
                click: function($event) {
                  _vm.toggleOption(option)
                }
              }
            },
            [
              _c("Checkbox", {
                attrs: { checked: _vm.value.indexOf(option) !== -1 }
              }),
              _vm._v("\n      " + _vm._s(option.label) + "\n    ")
            ],
            1
          )
        })
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true


// CONCATENATED MODULE: ./src/patterns/Picker.vue?vue&type=template&id=509124de&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/patterns/Picker.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var handleBlurAction; // used for v-closeable directive

/**
 * Picker is generally a selection form control allowing for either single or multiple
 * values selection. Actual values might differ from their labels.
 */

/* harmony default export */ var Pickervue_type_script_lang_js_ = ({
  name: "Picker",
  status: "prototype",
  release: "1.0.0",
  props: {
    /**
     * Short phrase displayed on a picker when
     * no celection is made.
     */
    greeter: {
      type: String,
      required: false,
      default: "Wybierz..."
    },

    /**
     * The html element used for the button.
     * `[{value: 1, label: 'Option One'}, {value: 2, label: 'Option Two'}]`
     */
    options: {
      type: Array,
      required: true,
      default: []
    }
  },
  data: function data() {
    return {
      open: false,
      value: []
    };
  },
  methods: {
    toggleVisibility: function toggleVisibility() {
      this.open = !this.open;
    },
    openPicker: function openPicker() {
      this.open = true;
    },
    closePicker: function closePicker() {
      this.open = false;
    },
    onBlur: function onBlur() {
      this.closePicker();
    },
    toggleOption: function toggleOption(option) {
      var itemIndex = this.value.indexOf(option);

      if (itemIndex !== -1) {
        this.value.splice(itemIndex, 1);
      } else {
        this.value.push(option);
      }

      this.$emit("update", this.value.map(function (item) {
        return item.value;
      }));
    }
  },
  computed: {
    displayValue: function displayValue() {
      return this.value.filter(function (item, index) {
        return index < 3;
      });
    }
  },
  directives: {
    closable: {
      bind: function bind(el, binding, vnode) {
        handleBlurAction = function handleBlurAction(e) {
          e.stopPropagation();
          var _binding$value = binding.value,
              handler = _binding$value.handler,
              exclude = _binding$value.exclude;
          var clickedOnExcludedEl = false;
          exclude.forEach(function (refName) {
            if (!clickedOnExcludedEl) {
              var excludedEl = vnode.context.$refs[refName];
              clickedOnExcludedEl = excludedEl.contains(e.target);
            }
          });

          if (!el.contains(e.target) && !clickedOnExcludedEl) {
            vnode.context[handler]();
          }
        };

        document.addEventListener("click", handleBlurAction);
        document.addEventListener("touchstart", handleBlurAction);
      },
      unbind: function unbind() {
        document.removeEventListener("click", handleBlurAction);
        document.removeEventListener("touchstart", handleBlurAction);
      }
    }
  }
});
// CONCATENATED MODULE: ./src/patterns/Picker.vue?vue&type=script&lang=js&
 /* harmony default export */ var patterns_Pickervue_type_script_lang_js_ = (Pickervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/patterns/Picker.vue?vue&type=style&index=0&id=509124de&lang=scss&scoped=true&
var Pickervue_type_style_index_0_id_509124de_lang_scss_scoped_true_ = __webpack_require__("EFq2");

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("KHd+");

// EXTERNAL MODULE: ./src/patterns/Picker.vue?vue&type=custom&index=0&blockType=docs
var Pickervue_type_custom_index_0_blockType_docs = __webpack_require__("/gSI");
var Pickervue_type_custom_index_0_blockType_docs_default = /*#__PURE__*/__webpack_require__.n(Pickervue_type_custom_index_0_blockType_docs);

// CONCATENATED MODULE: ./src/patterns/Picker.vue






/* normalize component */

var component = Object(componentNormalizer["default"])(
  patterns_Pickervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  "509124de",
  null
  
)

/* custom blocks */

if (typeof Pickervue_type_custom_index_0_blockType_docs_default.a === 'function') Pickervue_type_custom_index_0_blockType_docs_default()(component)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/patterns/Picker.vue"
/* harmony default export */ var Picker = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "ECTP":
/*!***************************************************************************!*\
  !*** ./src/elements/Paragraph.vue?vue&type=custom&index=0&blockType=docs ***!
  \***************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports) {



/***/ }),

/***/ "EFq2":
/*!*******************************************************************************************!*\
  !*** ./src/patterns/Picker.vue?vue&type=style&index=0&id=509124de&lang=scss&scoped=true& ***!
  \*******************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module exports are unknown */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Picker_vue_vue_type_style_index_0_id_509124de_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/mini-css-extract-plugin/dist/loader.js!../../node_modules/css-loader??ref--9-1!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src??ref--9-2!../../node_modules/sass-loader/lib/loader.js??ref--9-3!../../node_modules/sass-resources-loader/lib/loader.js??ref--9-4!../../node_modules/vue-loader/lib??vue-loader-options!./Picker.vue?vue&type=style&index=0&id=509124de&lang=scss&scoped=true& */ "b2RI");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Picker_vue_vue_type_style_index_0_id_509124de_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Picker_vue_vue_type_style_index_0_id_509124de_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Picker_vue_vue_type_style_index_0_id_509124de_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Picker_vue_vue_type_style_index_0_id_509124de_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Picker_vue_vue_type_style_index_0_id_509124de_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "FmFd":
/*!************************************************!*\
  !*** ./src/elements/Paragraph.vue + 4 modules ***!
  \************************************************/
/*! exports provided: default */
/*! ModuleConcatenation bailout: Cannot concat with ./src/elements/Paragraph.vue?vue&type=custom&index=0&blockType=docs (<- Module is not an ECMAScript module) */
/*! ModuleConcatenation bailout: Cannot concat with ./node_modules/vue-loader/lib/runtime/componentNormalizer.js because of ./src/elements/Button.vue */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Paragraph.vue?vue&type=template&id=5a8076cf&scoped=true&
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    _vm.type,
    { tag: "component", class: ["paragraph", _vm.variation] },
    [_vm._t("default")],
    2
  )
}
var staticRenderFns = []
render._withStripped = true


// CONCATENATED MODULE: ./src/elements/Paragraph.vue?vue&type=template&id=5a8076cf&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Paragraph.vue?vue&type=script&lang=js&
//
//
//
//
/* harmony default export */ var Paragraphvue_type_script_lang_js_ = ({
  name: "Paragraph",
  status: "ready",
  release: "1.0.0",
  props: {
    /**
     * The html element used for the text.
     * `p, span`
     */
    type: {
      type: String,
      default: "p",
      validator: function validator(value) {
        return value.match(/(p|span)/);
      }
    },

    /**
     * Style variation to give additional meaning.
     * `lead, small, medium, large`
     */
    variation: {
      type: String,
      default: "medium",
      validator: function validator(value) {
        return value.match(/(medium|lead|large|small)/);
      }
    }
  }
});
// CONCATENATED MODULE: ./src/elements/Paragraph.vue?vue&type=script&lang=js&
 /* harmony default export */ var elements_Paragraphvue_type_script_lang_js_ = (Paragraphvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/elements/Paragraph.vue?vue&type=style&index=0&id=5a8076cf&lang=scss&scoped=true&
var Paragraphvue_type_style_index_0_id_5a8076cf_lang_scss_scoped_true_ = __webpack_require__("Z7sc");

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("KHd+");

// EXTERNAL MODULE: ./src/elements/Paragraph.vue?vue&type=custom&index=0&blockType=docs
var Paragraphvue_type_custom_index_0_blockType_docs = __webpack_require__("ECTP");
var Paragraphvue_type_custom_index_0_blockType_docs_default = /*#__PURE__*/__webpack_require__.n(Paragraphvue_type_custom_index_0_blockType_docs);

// CONCATENATED MODULE: ./src/elements/Paragraph.vue






/* normalize component */

var component = Object(componentNormalizer["default"])(
  elements_Paragraphvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  "5a8076cf",
  null
  
)

/* custom blocks */

if (typeof Paragraphvue_type_custom_index_0_blockType_docs_default.a === 'function') Paragraphvue_type_custom_index_0_blockType_docs_default()(component)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/elements/Paragraph.vue"
/* harmony default export */ var Paragraph = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "Hn6g":
/*!*******************************************************************************!*\
  !*** ./src/patterns/CheckboxInput.vue?vue&type=custom&index=0&blockType=docs ***!
  \*******************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports) {



/***/ }),

/***/ "IFMf":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader??ref--9-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/lib/loader.js??ref--9-3!./node_modules/sass-resources-loader/lib/loader.js??ref--9-4!./node_modules/vue-loader/lib??vue-loader-options!./src/patterns/CheckboxInput.vue?vue&type=style&index=0&id=a37ac5d8&lang=scss&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "IQPs":
/*!************************************************************************************************!*\
  !*** ./src/patterns/FeatureCard.vue?vue&type=style&index=0&id=29703033&lang=scss&scoped=true& ***!
  \************************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module exports are unknown */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_FeatureCard_vue_vue_type_style_index_0_id_29703033_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/mini-css-extract-plugin/dist/loader.js!../../node_modules/css-loader??ref--9-1!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src??ref--9-2!../../node_modules/sass-loader/lib/loader.js??ref--9-3!../../node_modules/sass-resources-loader/lib/loader.js??ref--9-4!../../node_modules/vue-loader/lib??vue-loader-options!./FeatureCard.vue?vue&type=style&index=0&id=29703033&lang=scss&scoped=true& */ "A5wI");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_FeatureCard_vue_vue_type_style_index_0_id_29703033_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_FeatureCard_vue_vue_type_style_index_0_id_29703033_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_FeatureCard_vue_vue_type_style_index_0_id_29703033_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_FeatureCard_vue_vue_type_style_index_0_id_29703033_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_FeatureCard_vue_vue_type_style_index_0_id_29703033_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "IdG1":
/*!****************************************************!*\
  !*** ./src/patterns/CheckboxInput.vue + 4 modules ***!
  \****************************************************/
/*! exports provided: default */
/*! ModuleConcatenation bailout: Cannot concat with ./src/patterns/CheckboxInput.vue?vue&type=custom&index=0&blockType=docs (<- Module is not an ECMAScript module) */
/*! ModuleConcatenation bailout: Cannot concat with ./node_modules/vue-loader/lib/runtime/componentNormalizer.js because of ./src/elements/Button.vue */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/patterns/CheckboxInput.vue?vue&type=template&id=a37ac5d8&scoped=true&
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "checkbox-input" }, [
    _c(
      "label",
      [
        _c(
          "span",
          { staticClass: "checker" },
          [
            _c("input", {
              attrs: {
                type: "checkbox",
                name: _vm.name,
                required: _vm.required
              },
              domProps: { checked: _vm.value }
            }),
            _vm._v(" "),
            _c("Checkbox", { attrs: { checked: _vm.value } })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "Paragraph",
          { attrs: { type: "span", variation: "small" } },
          [_vm._t("default")],
          2
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true


// CONCATENATED MODULE: ./src/patterns/CheckboxInput.vue?vue&type=template&id=a37ac5d8&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/patterns/CheckboxInput.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//

/**
 * Form Inputs are used to allow users to provide text input when the expected
 * input is short. Form Input has a range of options and supports several text
 * formats including numbers. For longer input, use the form `Textarea` element.
 */
/* harmony default export */ var CheckboxInputvue_type_script_lang_js_ = ({
  name: "CheckboxInput",
  status: "prototype",
  release: "1.0.0",
  props: {
    /**
     * Unique identifier of the form input field.
     */
    id: {
      type: String,
      default: null
    },

    /**
     * Form control value/state.
     * `true, false`
     */
    value: {
      type: Boolean,
      required: false,
      default: false
    },

    /**
     * Determines if checking the field is required
     * to submit the form.
     * `true, false`
     */
    required: {
      type: Boolean,
      required: false,
      default: false
    }
  },
  methods: {
    update: function update(value) {
      this.$emit("update", value);
    }
  }
});
// CONCATENATED MODULE: ./src/patterns/CheckboxInput.vue?vue&type=script&lang=js&
 /* harmony default export */ var patterns_CheckboxInputvue_type_script_lang_js_ = (CheckboxInputvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/patterns/CheckboxInput.vue?vue&type=style&index=0&id=a37ac5d8&lang=scss&scoped=true&
var CheckboxInputvue_type_style_index_0_id_a37ac5d8_lang_scss_scoped_true_ = __webpack_require__("R+cg");

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("KHd+");

// EXTERNAL MODULE: ./src/patterns/CheckboxInput.vue?vue&type=custom&index=0&blockType=docs
var CheckboxInputvue_type_custom_index_0_blockType_docs = __webpack_require__("Hn6g");
var CheckboxInputvue_type_custom_index_0_blockType_docs_default = /*#__PURE__*/__webpack_require__.n(CheckboxInputvue_type_custom_index_0_blockType_docs);

// CONCATENATED MODULE: ./src/patterns/CheckboxInput.vue






/* normalize component */

var component = Object(componentNormalizer["default"])(
  patterns_CheckboxInputvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  "a37ac5d8",
  null
  
)

/* custom blocks */

if (typeof CheckboxInputvue_type_custom_index_0_blockType_docs_default.a === 'function') CheckboxInputvue_type_custom_index_0_blockType_docs_default()(component)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/patterns/CheckboxInput.vue"
/* harmony default export */ var CheckboxInput = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "JtIp":
/*!**********************************!*\
  !*** ./src/patterns sync \.vue$ ***!
  \**********************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./CheckboxInput.vue": "IdG1",
	"./FeatureCard.vue": "WjFP",
	"./Picker.vue": "Cs02"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "JtIp";

/***/ }),

/***/ "KHd+":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "KzwA":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader??ref--9-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/lib/loader.js??ref--9-3!./node_modules/sass-resources-loader/lib/loader.js??ref--9-4!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Icon.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "NcSd":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader??ref--9-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/lib/loader.js??ref--9-3!./node_modules/sass-resources-loader/lib/loader.js??ref--9-4!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Heading.vue?vue&type=style&index=0&id=f818c0ba&lang=scss&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "R+cg":
/*!**************************************************************************************************!*\
  !*** ./src/patterns/CheckboxInput.vue?vue&type=style&index=0&id=a37ac5d8&lang=scss&scoped=true& ***!
  \**************************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module exports are unknown */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_CheckboxInput_vue_vue_type_style_index_0_id_a37ac5d8_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/mini-css-extract-plugin/dist/loader.js!../../node_modules/css-loader??ref--9-1!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src??ref--9-2!../../node_modules/sass-loader/lib/loader.js??ref--9-3!../../node_modules/sass-resources-loader/lib/loader.js??ref--9-4!../../node_modules/vue-loader/lib??vue-loader-options!./CheckboxInput.vue?vue&type=style&index=0&id=a37ac5d8&lang=scss&scoped=true& */ "IFMf");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_CheckboxInput_vue_vue_type_style_index_0_id_a37ac5d8_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_CheckboxInput_vue_vue_type_style_index_0_id_a37ac5d8_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_CheckboxInput_vue_vue_type_style_index_0_id_a37ac5d8_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_CheckboxInput_vue_vue_type_style_index_0_id_a37ac5d8_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_CheckboxInput_vue_vue_type_style_index_0_id_a37ac5d8_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "SPA1":
/*!***********************************************!*\
  !*** ./src/elements/Checkbox.vue + 4 modules ***!
  \***********************************************/
/*! exports provided: default */
/*! ModuleConcatenation bailout: Cannot concat with ./src/elements/Checkbox.vue?vue&type=custom&index=0&blockType=docs (<- Module is not an ECMAScript module) */
/*! ModuleConcatenation bailout: Cannot concat with ./node_modules/vue-loader/lib/runtime/componentNormalizer.js because of ./src/elements/Button.vue */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Checkbox.vue?vue&type=template&id=3627f5f2&scoped=true&
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("i", {
    staticClass: "checkbox",
    class: { checked: _vm.checked },
    on: { click: _vm.toggle }
  })
}
var staticRenderFns = []
render._withStripped = true


// CONCATENATED MODULE: ./src/elements/Checkbox.vue?vue&type=template&id=3627f5f2&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Checkbox.vue?vue&type=script&lang=js&
//
//
//
//

/**
 * Checkbox represent a single boolean state.
 */
/* harmony default export */ var Checkboxvue_type_script_lang_js_ = ({
  name: "Checkbox",
  status: "ready",
  release: "1.0.0",
  props: {
    /**
     * Indicates current checkbox state.
     */
    checked: {
      type: Boolean,
      required: true,
      default: false
    }
  },
  methods: {
    toggle: function toggle() {
      this.$emit("toggle");
    }
  }
});
// CONCATENATED MODULE: ./src/elements/Checkbox.vue?vue&type=script&lang=js&
 /* harmony default export */ var elements_Checkboxvue_type_script_lang_js_ = (Checkboxvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/elements/Checkbox.vue?vue&type=style&index=0&id=3627f5f2&lang=scss&scoped=true&
var Checkboxvue_type_style_index_0_id_3627f5f2_lang_scss_scoped_true_ = __webpack_require__("itza");

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("KHd+");

// EXTERNAL MODULE: ./src/elements/Checkbox.vue?vue&type=custom&index=0&blockType=docs
var Checkboxvue_type_custom_index_0_blockType_docs = __webpack_require__("gkox");
var Checkboxvue_type_custom_index_0_blockType_docs_default = /*#__PURE__*/__webpack_require__.n(Checkboxvue_type_custom_index_0_blockType_docs);

// CONCATENATED MODULE: ./src/elements/Checkbox.vue






/* normalize component */

var component = Object(componentNormalizer["default"])(
  elements_Checkboxvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  "3627f5f2",
  null
  
)

/* custom blocks */

if (typeof Checkboxvue_type_custom_index_0_blockType_docs_default.a === 'function') Checkboxvue_type_custom_index_0_blockType_docs_default()(component)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/elements/Checkbox.vue"
/* harmony default export */ var Checkbox = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "UklV":
/*!********************************************!*\
  !*** ./src/elements/Input.vue + 4 modules ***!
  \********************************************/
/*! exports provided: default */
/*! ModuleConcatenation bailout: Cannot concat with ./src/elements/Input.vue?vue&type=custom&index=0&blockType=docs (<- Module is not an ECMAScript module) */
/*! ModuleConcatenation bailout: Cannot concat with ./node_modules/vue-loader/lib/runtime/componentNormalizer.js because of ./src/elements/Button.vue */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Input.vue?vue&type=template&id=0c3078aa&scoped=true&
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    _vm.wrapper,
    {
      tag: "component",
      class: [
        "input",
        {
          "input-expand": _vm.width === "expand",
          "no-label-on-desktop": _vm.noLabelOnDesktop
        }
      ]
    },
    [
      _vm.label
        ? _c("label", { attrs: { for: _vm.id } }, [_vm._v(_vm._s(_vm.label))])
        : _vm._e(),
      _vm._v(" "),
      _c("input", {
        class: _vm.state,
        attrs: {
          id: _vm.id,
          name: _vm.name,
          disabled: _vm.disabled,
          type: _vm.type,
          placeholder: _vm.placeholder,
          minlength: _vm.minlength,
          maxlength: _vm.maxlength,
          required: _vm.required
        },
        domProps: { value: _vm.value },
        on: {
          input: function($event) {
            _vm.onInput($event.target.value)
          },
          focus: function($event) {
            _vm.onFocus($event.target.value)
          }
        }
      })
    ]
  )
}
var staticRenderFns = []
render._withStripped = true


// CONCATENATED MODULE: ./src/elements/Input.vue?vue&type=template&id=0c3078aa&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Input.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/**
 * Form Inputs are used to allow users to provide text input when the expected
 * input is short. Form Input has a range of options and supports several text
 * formats including numbers. For longer input, use the form `Textarea` element.
 */
/* harmony default export */ var Inputvue_type_script_lang_js_ = ({
  name: "Input",
  status: "ready",
  release: "1.0.0",
  props: {
    /**
     * The type of the form input field.
     * `text, number, email`
     */
    type: {
      type: String,
      default: "text",
      validator: function validator(value) {
        return value.match(/(text|number|email)/);
      }
    },

    /**
     * Text value of the form input field.
     */
    value: {
      type: String,
      default: null
    },

    /**
     * The placeholder value for the form input field.
     */
    placeholder: {
      type: String,
      default: null
    },

    /**
     * The label of the form input field.
     */
    label: {
      type: String,
      default: null
    },

    /**
     * The html element name used for the wrapper.
     * `div, section`
     */
    wrapper: {
      type: String,
      default: "div",
      validator: function validator(value) {
        return value.match(/(div|section)/);
      }
    },

    /**
     * Unique selector of the form input field.
     */
    id: {
      type: String,
      default: null
    },

    /**
     * Unique name of the form input field.
     */
    name: {
      type: String,
      required: true
    },

    /**
     * The width of the form input field.
     * `auto, expand`
     */
    width: {
      type: String,
      default: "expand",
      validator: function validator(value) {
        return value.match(/(auto|expand)/);
      }
    },

    /**
     * Whether the form input field is disabled or not.
     * `true, false`
     */
    disabled: {
      type: Boolean,
      default: false
    },

    /**
     * Manually trigger various states of the input.
     * `hover, active, focus`
     */
    state: {
      type: String,
      default: null,
      validator: function validator(value) {
        return value.match(/(hover|active|focus)/);
      }
    },

    /**
     * Determines if field value must be provided for
     * the form to be submitted.
     */
    required: {
      type: Boolean,
      default: false
    },

    /**
     * Minimal characters length of field value.
     */
    minlength: {
      type: Number,
      default: null
    },

    /**
     * Maximal characters length of field value.
     */
    maxlength: {
      type: Number,
      default: null
    },

    /**
     * If true, label will not be rendered on desktop.
     */
    noLabelOnDesktop: {
      type: Boolean,
      default: false
    }
  },
  methods: {
    onInput: function onInput(value) {
      this.$emit("change", value);
    },
    onFocus: function onFocus(value) {
      this.$emit("focus", value);
    }
  }
});
// CONCATENATED MODULE: ./src/elements/Input.vue?vue&type=script&lang=js&
 /* harmony default export */ var elements_Inputvue_type_script_lang_js_ = (Inputvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/elements/Input.vue?vue&type=style&index=0&id=0c3078aa&lang=scss&scoped=true&
var Inputvue_type_style_index_0_id_0c3078aa_lang_scss_scoped_true_ = __webpack_require__("5WVf");

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("KHd+");

// EXTERNAL MODULE: ./src/elements/Input.vue?vue&type=custom&index=0&blockType=docs
var Inputvue_type_custom_index_0_blockType_docs = __webpack_require__("7dOS");
var Inputvue_type_custom_index_0_blockType_docs_default = /*#__PURE__*/__webpack_require__.n(Inputvue_type_custom_index_0_blockType_docs);

// CONCATENATED MODULE: ./src/elements/Input.vue






/* normalize component */

var component = Object(componentNormalizer["default"])(
  elements_Inputvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  "0c3078aa",
  null
  
)

/* custom blocks */

if (typeof Inputvue_type_custom_index_0_blockType_docs_default.a === 'function') Inputvue_type_custom_index_0_blockType_docs_default()(component)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/elements/Input.vue"
/* harmony default export */ var Input = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "Wg5T":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader??ref--9-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/lib/loader.js??ref--9-3!./node_modules/sass-resources-loader/lib/loader.js??ref--9-4!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Input.vue?vue&type=style&index=0&id=0c3078aa&lang=scss&scoped=true& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "WjFP":
/*!**************************************************!*\
  !*** ./src/patterns/FeatureCard.vue + 4 modules ***!
  \**************************************************/
/*! exports provided: default */
/*! ModuleConcatenation bailout: Cannot concat with ./src/patterns/FeatureCard.vue?vue&type=custom&index=0&blockType=docs (<- Module is not an ECMAScript module) */
/*! ModuleConcatenation bailout: Cannot concat with ./node_modules/vue-loader/lib/runtime/componentNormalizer.js because of ./src/elements/Button.vue */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/patterns/FeatureCard.vue?vue&type=template&id=29703033&scoped=true&
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "feature-card" },
    [
      _c("div", { staticClass: "feature-card__image" }, [
        _vm.image ? _c("img", { attrs: { src: _vm.image } }) : _vm._e()
      ]),
      _vm._v(" "),
      _vm.heading
        ? _c("Heading", { attrs: { level: "h4", align: "center" } }, [
            _vm._v(_vm._s(_vm.heading))
          ])
        : _vm._e(),
      _vm._v(" "),
      _c("Paragraph", [_vm._t("default")], 2)
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true


// CONCATENATED MODULE: ./src/patterns/FeatureCard.vue?vue&type=template&id=29703033&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/patterns/FeatureCard.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//

/**
 * Feature card is used to present some key value of a product
 * or feature as a composite element. Best to be used in multicolumn
 * layout.
 */
/* harmony default export */ var FeatureCardvue_type_script_lang_js_ = ({
  name: "FeatureCard",
  status: "ready",
  release: "1.0.0",
  props: {
    /**
     * Path to feature image.
     */
    image: {
      type: String,
      default: null
    },

    /**
     * Heading text content.
     */
    heading: {
      type: String,
      default: null
    }
  }
});
// CONCATENATED MODULE: ./src/patterns/FeatureCard.vue?vue&type=script&lang=js&
 /* harmony default export */ var patterns_FeatureCardvue_type_script_lang_js_ = (FeatureCardvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/patterns/FeatureCard.vue?vue&type=style&index=0&id=29703033&lang=scss&scoped=true&
var FeatureCardvue_type_style_index_0_id_29703033_lang_scss_scoped_true_ = __webpack_require__("IQPs");

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("KHd+");

// EXTERNAL MODULE: ./src/patterns/FeatureCard.vue?vue&type=custom&index=0&blockType=docs
var FeatureCardvue_type_custom_index_0_blockType_docs = __webpack_require__("hUbm");
var FeatureCardvue_type_custom_index_0_blockType_docs_default = /*#__PURE__*/__webpack_require__.n(FeatureCardvue_type_custom_index_0_blockType_docs);

// CONCATENATED MODULE: ./src/patterns/FeatureCard.vue






/* normalize component */

var component = Object(componentNormalizer["default"])(
  patterns_FeatureCardvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  "29703033",
  null
  
)

/* custom blocks */

if (typeof FeatureCardvue_type_custom_index_0_blockType_docs_default.a === 'function') FeatureCardvue_type_custom_index_0_blockType_docs_default()(component)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/patterns/FeatureCard.vue"
/* harmony default export */ var FeatureCard = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "Xl01":
/*!**********************************!*\
  !*** ./src/elements sync \.vue$ ***!
  \**********************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./Button.vue": "/kPn",
	"./Checkbox.vue": "SPA1",
	"./Heading.vue": "oIR/",
	"./Icon.vue": "68yd",
	"./Input.vue": "UklV",
	"./Paragraph.vue": "FmFd",
	"./Toggle.vue": "AsAg"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "Xl01";

/***/ }),

/***/ "Z7sc":
/*!**********************************************************************************************!*\
  !*** ./src/elements/Paragraph.vue?vue&type=style&index=0&id=5a8076cf&lang=scss&scoped=true& ***!
  \**********************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module exports are unknown */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Paragraph_vue_vue_type_style_index_0_id_5a8076cf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/mini-css-extract-plugin/dist/loader.js!../../node_modules/css-loader??ref--9-1!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src??ref--9-2!../../node_modules/sass-loader/lib/loader.js??ref--9-3!../../node_modules/sass-resources-loader/lib/loader.js??ref--9-4!../../node_modules/vue-loader/lib??vue-loader-options!./Paragraph.vue?vue&type=style&index=0&id=5a8076cf&lang=scss&scoped=true& */ "+aZc");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Paragraph_vue_vue_type_style_index_0_id_5a8076cf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Paragraph_vue_vue_type_style_index_0_id_5a8076cf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Paragraph_vue_vue_type_style_index_0_id_5a8076cf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Paragraph_vue_vue_type_style_index_0_id_5a8076cf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Paragraph_vue_vue_type_style_index_0_id_5a8076cf_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "b2RI":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader??ref--9-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/lib/loader.js??ref--9-3!./node_modules/sass-resources-loader/lib/loader.js??ref--9-4!./node_modules/vue-loader/lib??vue-loader-options!./src/patterns/Picker.vue?vue&type=style&index=0&id=509124de&lang=scss&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "bzL3":
/*!*********************************************!*\
  !*** ./src/assets/icons sync ^\.\/.*\.svg$ ***!
  \*********************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./deprecated.svg": "/thn",
	"./prototype.svg": "2dwG",
	"./ready.svg": "AAsE",
	"./review.svg": "eAtJ"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "bzL3";

/***/ }),

/***/ "c1KM":
/*!***********************************!*\
  !*** ./src/templates sync \.vue$ ***!
  \***********************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./FeatureSection.vue": "+KGH"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "c1KM";

/***/ }),

/***/ "c5QI":
/*!*****************************************************************!*\
  !*** ./src/elements/Icon.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module exports are unknown */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Icon_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/mini-css-extract-plugin/dist/loader.js!../../node_modules/css-loader??ref--9-1!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src??ref--9-2!../../node_modules/sass-loader/lib/loader.js??ref--9-3!../../node_modules/sass-resources-loader/lib/loader.js??ref--9-4!../../node_modules/vue-loader/lib??vue-loader-options!./Icon.vue?vue&type=style&index=0&lang=scss& */ "KzwA");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Icon_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Icon_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Icon_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Icon_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Icon_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "cNLi":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader??ref--9-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/lib/loader.js??ref--9-3!./node_modules/sass-resources-loader/lib/loader.js??ref--9-4!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Toggle.vue?vue&type=style&index=0&id=7f8a827a&lang=scss&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "eAtJ":
/*!*************************************!*\
  !*** ./src/assets/icons/review.svg ***!
  \*************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 576 512\"><path d=\"M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z\"/></svg>";

/***/ }),

/***/ "gkox":
/*!**************************************************************************!*\
  !*** ./src/elements/Checkbox.vue?vue&type=custom&index=0&blockType=docs ***!
  \**************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports) {



/***/ }),

/***/ "hUbm":
/*!*****************************************************************************!*\
  !*** ./src/patterns/FeatureCard.vue?vue&type=custom&index=0&blockType=docs ***!
  \*****************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports) {



/***/ }),

/***/ "iBhG":
/*!*********************************************************************************!*\
  !*** ./src/templates/FeatureSection.vue?vue&type=custom&index=0&blockType=docs ***!
  \*********************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports) {



/***/ }),

/***/ "itza":
/*!*********************************************************************************************!*\
  !*** ./src/elements/Checkbox.vue?vue&type=style&index=0&id=3627f5f2&lang=scss&scoped=true& ***!
  \*********************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module exports are unknown */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Checkbox_vue_vue_type_style_index_0_id_3627f5f2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/mini-css-extract-plugin/dist/loader.js!../../node_modules/css-loader??ref--9-1!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src??ref--9-2!../../node_modules/sass-loader/lib/loader.js??ref--9-3!../../node_modules/sass-resources-loader/lib/loader.js??ref--9-4!../../node_modules/vue-loader/lib??vue-loader-options!./Checkbox.vue?vue&type=style&index=0&id=3627f5f2&lang=scss&scoped=true& */ "qV1O");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Checkbox_vue_vue_type_style_index_0_id_3627f5f2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Checkbox_vue_vue_type_style_index_0_id_3627f5f2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Checkbox_vue_vue_type_style_index_0_id_3627f5f2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Checkbox_vue_vue_type_style_index_0_id_3627f5f2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Checkbox_vue_vue_type_style_index_0_id_3627f5f2_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "jDvS":
/*!*******************************************************************************************!*\
  !*** ./src/elements/Toggle.vue?vue&type=style&index=0&id=7f8a827a&lang=scss&scoped=true& ***!
  \*******************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module exports are unknown */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Toggle_vue_vue_type_style_index_0_id_7f8a827a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/mini-css-extract-plugin/dist/loader.js!../../node_modules/css-loader??ref--9-1!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src??ref--9-2!../../node_modules/sass-loader/lib/loader.js??ref--9-3!../../node_modules/sass-resources-loader/lib/loader.js??ref--9-4!../../node_modules/vue-loader/lib??vue-loader-options!./Toggle.vue?vue&type=style&index=0&id=7f8a827a&lang=scss&scoped=true& */ "cNLi");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Toggle_vue_vue_type_style_index_0_id_7f8a827a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Toggle_vue_vue_type_style_index_0_id_7f8a827a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Toggle_vue_vue_type_style_index_0_id_7f8a827a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Toggle_vue_vue_type_style_index_0_id_7f8a827a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Toggle_vue_vue_type_style_index_0_id_7f8a827a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "nJDe":
/*!************************************************************************!*\
  !*** ./src/elements/Toggle.vue?vue&type=custom&index=0&blockType=docs ***!
  \************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports) {



/***/ }),

/***/ "oIR/":
/*!**********************************************!*\
  !*** ./src/elements/Heading.vue + 4 modules ***!
  \**********************************************/
/*! exports provided: default */
/*! ModuleConcatenation bailout: Cannot concat with ./src/elements/Heading.vue?vue&type=custom&index=0&blockType=docs (<- Module is not an ECMAScript module) */
/*! ModuleConcatenation bailout: Cannot concat with ./node_modules/vue-loader/lib/runtime/componentNormalizer.js because of ./src/elements/Button.vue */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Heading.vue?vue&type=template&id=f818c0ba&scoped=true&
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    _vm.level,
    {
      tag: "component",
      staticClass: "heading",
      class: [_vm.align, { separator: _vm.separator }]
    },
    [_vm._t("default")],
    2
  )
}
var staticRenderFns = []
render._withStripped = true


// CONCATENATED MODULE: ./src/elements/Heading.vue?vue&type=template&id=f818c0ba&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Heading.vue?vue&type=script&lang=js&
//
//
//
//
//
//

/**
 * Headings are used as the titles of each major section of a page in the
 * interface. For example, templates generally use headings as their title.
 * Heading element provides an option to change the level of the heading.
 * There is also an optional subtle separator below the heading.
 */
/* harmony default export */ var Headingvue_type_script_lang_js_ = ({
  name: "Heading",
  status: "prototype",
  release: "1.0.0",
  props: {
    /**
     * The heading level used for the heading.
     * `h1, h2, h3, h4, h5, h6`
     */
    level: {
      type: String,
      default: "h1",
      validator: function validator(value) {
        return value.match(/(h1|h2|h3|h4|h5|h6)/);
      }
    },

    /**
     * Sets aligment of the heading contents.
     * `left, center, right`
     */
    align: {
      type: String,
      default: "left",
      validator: function validator(value) {
        return value.match(/(left|center|right)/);
      }
    },

    /**
     * Whether a subtle separator should be rendered below the heading.
     * `true, false`
     */
    separator: {
      type: Boolean,
      default: false
    }
  }
});
// CONCATENATED MODULE: ./src/elements/Heading.vue?vue&type=script&lang=js&
 /* harmony default export */ var elements_Headingvue_type_script_lang_js_ = (Headingvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/elements/Heading.vue?vue&type=style&index=0&id=f818c0ba&lang=scss&scoped=true&
var Headingvue_type_style_index_0_id_f818c0ba_lang_scss_scoped_true_ = __webpack_require__("qPyF");

// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__("KHd+");

// EXTERNAL MODULE: ./src/elements/Heading.vue?vue&type=custom&index=0&blockType=docs
var Headingvue_type_custom_index_0_blockType_docs = __webpack_require__("woMB");
var Headingvue_type_custom_index_0_blockType_docs_default = /*#__PURE__*/__webpack_require__.n(Headingvue_type_custom_index_0_blockType_docs);

// CONCATENATED MODULE: ./src/elements/Heading.vue






/* normalize component */

var component = Object(componentNormalizer["default"])(
  elements_Headingvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  "f818c0ba",
  null
  
)

/* custom blocks */

if (typeof Headingvue_type_custom_index_0_blockType_docs_default.a === 'function') Headingvue_type_custom_index_0_blockType_docs_default()(component)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/elements/Heading.vue"
/* harmony default export */ var Heading = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "qPyF":
/*!********************************************************************************************!*\
  !*** ./src/elements/Heading.vue?vue&type=style&index=0&id=f818c0ba&lang=scss&scoped=true& ***!
  \********************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module exports are unknown */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Heading_vue_vue_type_style_index_0_id_f818c0ba_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/mini-css-extract-plugin/dist/loader.js!../../node_modules/css-loader??ref--9-1!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src??ref--9-2!../../node_modules/sass-loader/lib/loader.js??ref--9-3!../../node_modules/sass-resources-loader/lib/loader.js??ref--9-4!../../node_modules/vue-loader/lib??vue-loader-options!./Heading.vue?vue&type=style&index=0&id=f818c0ba&lang=scss&scoped=true& */ "NcSd");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Heading_vue_vue_type_style_index_0_id_f818c0ba_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Heading_vue_vue_type_style_index_0_id_f818c0ba_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Heading_vue_vue_type_style_index_0_id_f818c0ba_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Heading_vue_vue_type_style_index_0_id_f818c0ba_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Heading_vue_vue_type_style_index_0_id_f818c0ba_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "qV1O":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader??ref--9-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--9-2!./node_modules/sass-loader/lib/loader.js??ref--9-3!./node_modules/sass-resources-loader/lib/loader.js??ref--9-4!./node_modules/vue-loader/lib??vue-loader-options!./src/elements/Checkbox.vue?vue&type=style&index=0&id=3627f5f2&lang=scss&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "tcIX":
/*!************************************************************************!*\
  !*** ./src/elements/Button.vue?vue&type=custom&index=0&blockType=docs ***!
  \************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports) {



/***/ }),

/***/ "u8pL":
/*!*******************************************************************************************!*\
  !*** ./src/elements/Button.vue?vue&type=style&index=0&id=08690ba1&lang=scss&scoped=true& ***!
  \*******************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module exports are unknown */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Button_vue_vue_type_style_index_0_id_08690ba1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/mini-css-extract-plugin/dist/loader.js!../../node_modules/css-loader??ref--9-1!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src??ref--9-2!../../node_modules/sass-loader/lib/loader.js??ref--9-3!../../node_modules/sass-resources-loader/lib/loader.js??ref--9-4!../../node_modules/vue-loader/lib??vue-loader-options!./Button.vue?vue&type=style&index=0&id=08690ba1&lang=scss&scoped=true& */ "9Hea");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Button_vue_vue_type_style_index_0_id_08690ba1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Button_vue_vue_type_style_index_0_id_08690ba1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Button_vue_vue_type_style_index_0_id_08690ba1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Button_vue_vue_type_style_index_0_id_08690ba1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_Button_vue_vue_type_style_index_0_id_08690ba1_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "uN3n":
/*!****************************************************************************************************!*\
  !*** ./src/templates/FeatureSection.vue?vue&type=style&index=0&id=508f391e&lang=scss&scoped=true& ***!
  \****************************************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module exports are unknown */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_FeatureSection_vue_vue_type_style_index_0_id_508f391e_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/mini-css-extract-plugin/dist/loader.js!../../node_modules/css-loader??ref--9-1!../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../node_modules/postcss-loader/src??ref--9-2!../../node_modules/sass-loader/lib/loader.js??ref--9-3!../../node_modules/sass-resources-loader/lib/loader.js??ref--9-4!../../node_modules/vue-loader/lib??vue-loader-options!./FeatureSection.vue?vue&type=style&index=0&id=508f391e&lang=scss&scoped=true& */ "8FB0");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_FeatureSection_vue_vue_type_style_index_0_id_508f391e_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_FeatureSection_vue_vue_type_style_index_0_id_508f391e_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_FeatureSection_vue_vue_type_style_index_0_id_508f391e_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_FeatureSection_vue_vue_type_style_index_0_id_508f391e_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_mini_css_extract_plugin_dist_loader_js_node_modules_css_loader_index_js_ref_9_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_9_2_node_modules_sass_loader_lib_loader_js_ref_9_3_node_modules_sass_resources_loader_lib_loader_js_ref_9_4_node_modules_vue_loader_lib_index_js_vue_loader_options_FeatureSection_vue_vue_type_style_index_0_id_508f391e_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "woMB":
/*!*************************************************************************!*\
  !*** ./src/elements/Heading.vue?vue&type=custom&index=0&blockType=docs ***!
  \*************************************************************************/
/*! no static exports found */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports) {



/***/ })

/******/ });